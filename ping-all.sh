#!/bin/bash
#

hosts=("2" "8" "9" "10" "11" "16" "17" "19" "20" "21" "22")
addrs=()
for host in "${hosts[@]}"; do
    addrs+=("10.24.0.${host}")
done

fping "${addrs[@]}" 2> /dev/null
