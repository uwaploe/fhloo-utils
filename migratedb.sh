#!/usr/bin/env bash
#
# Migrate the existing FHLOO InfuxDB database to a Docker container. The database
# user credentials must be stored in the file ./creds.env and containing the
# following environment variable settings:
#
# DB_USER=username
# DB_PASSWORD=userpassword
#
set -e

IMAGE="influxdb:alpine"

. creds.env

mkdir -p ~/fhloodb ~/influxdb
docker pull $IMAGE

# Dump the current DB contents to the directory ~/fhloodb
influxd backup -database fhloo ~/fhloodb

# Start the container and add a database and a user account. The system
# state for the container will be stored in ~/influxdb
docker run --rm \
  -e INFLUXDB_DB=fhloo \
  -e INFLUXDB_USER="$DB_USER" \
  -e INFLUXDB_USER_PASSWORD="$DB_PASSWORD" \
  -v ~/influxdb:/var/lib/influxdb $IMAGE /init-influxdb.sh

# Restore the saved meta-data
docker run --rm \
  -v ~/influxdb:/var/lib/influxdb \
  -v ~/fhloodb:/backup $IMAGE \
  /usr/bin/influxd restore -metadir /var/lib/influxdb/meta /backup

# Restore the saved DB contents
docker run --rm \
  -v ~/influxdb:/var/lib/influxdb \
  -v ~/fhloodb:/backup $IMAGE \
  /usr/bin/influxd restore -database fhloo -datadir /var/lib/influxdb/data /backup

echo "InfluxDB container ready"
echo "Start with: docker run -d -v ~/influxdb:/var/lib/influxdb $IMAGE"
