#!/usr/bin/env bash
#
# Synchronize the data archive mirror at FHL with the local contents
#

: ${FHLARCHIVE="fhlooapl@140.142.199.124:Data/"}
: ${DATADIR=$HOME/data}
: ${LOGDIR=$HOME/logs}

mkdir -p $LOGDIR
logfile=$LOGDIR/sync.log

echo "$(date -u +%FT%T)" >> $logfile
cd $DATADIR && \
    rsync -avX * $FHLARCHIVE 1>> $logfile 2>&1
