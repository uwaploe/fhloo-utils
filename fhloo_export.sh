#!/usr/bin/env bash
#
# Export FHLOO data from InfluxDB
#

# Docker network
: ${NETWORK=fhloo_fhlnet}
# Docker image
: ${IMAGE="fhloo/exporter:latest"}
# Top level data directory
: ${DATADIR=$HOME/data}

tstart="$1"
[[ -z "$tstart" ]] && {
    echo "Usage: ${0##*/} Tstart [Tend]" 1>&2
    exit 1
}

tend="$2"
[[ -z "$tend" ]] && tend="$(date -u +%Y-%m-%dT%H:%M:%S)"

user=
pword=
if [[ -f $HOME/.fhldb ]]; then
    IFS=: read user pword < $HOME/.fhldb
fi

docker run --rm \
       --network $NETWORK \
       -v "${DATADIR}:/data" \
       -e "DB_USER=$user" \
       -e "DB_PASSWORD=$pword" \
       -e "DB_URL=http://db:8086" \
       $IMAGE "$@"
