#!/bin/sh

if [ -n "$DB_USER" -a -n "$DB_PASSWORD" ]; then
    exec fhldata --user $DB_USER --pword $DB_PASSWORD "$@"
fi

exec "$@"
