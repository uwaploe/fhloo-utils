#!/usr/bin/env bash
#
# Export FHLOO data from InfluxDB
#
PATH=$HOME/bin:$PATH

: ${TOPDIR=$HOME/data}

declare -A TYPES
SENSORS=("ctd" "adcp" "sami_ph" "sami_co2")
TYPES=(
    ["ctd"]="csv"
    ["adcp"]="binary"
    ["sami_ph"]="text"
    ["sami_co2"]="text"
)


creds=()
if [[ -f $HOME/.fhldb ]]; then
    user=
    pword=
    IFS=: read user pword < $HOME/.fhldb
    creds=(-c "${user}:${pword}")
fi

tstart="$1"
tend="$2"
[[ -z "$tend" ]] && tend="$(date -u +%Y-%m-%dT%H:%M:%S)"

datadir="${TOPDIR}/$(date -u +'%Y/%j')"
mkdir -p "$datadir"
for sensor in "${SENSORS[@]}"; do
    case "${TYPES[$sensor]}" in
        csv)
            fname="${sensor}_$(date -u +%Y%m%d).csv"
            tmpfile="$datadir/.${fname}.tmp"
            if [[ -s "$datadir/$fname" ]]; then
                header="--header=false"
                cp "$datadir/$fname" "$tmpfile"
            else
                header="--header=true"
            fi
            tsdata "${creds[@]}" get --tstart "$tstart" --tend "$tend" \
                   $header fhloo "$sensor" >> "$tmpfile"
            # Ensure file is updated atomically
            mv "$tmpfile" "$datadir/$fname"
        ;;
        binary)
            tsdata "${creds[@]}" getbin --tstart "$tstart" --tend "$tend" \
               --outdir "$datadir/${sensor}.d" \
               fhloo "$sensor"
        ;;
        text)
            fname="${sensor}_$(date -u +%Y%m%d).txt"
            tmpfile="$datadir/.${fname}.tmp"
            if [[ -e "$datadir/$fname" ]]; then
                header="--header=false"
                cp "$datadir/$fname" "$tmpfile"
            else
                header="--header=true"
            fi
            tsdata "${creds[@]}" gettext --tstart "$tstart" --tend "$tend" \
                   fhloo "$sensor" >> "$tmpfile"
            # Ensure file is updated atomically
            mv "$tmpfile" "$datadir/$fname"
        ;;
    esac

done
