#!/usr/bin/env bash
#
# Access Redis running in a Docker container
#
: ${CONTAINER=fhloo_redis_1}

docker exec -it \
       $CONTAINER redis-cli "$@"
