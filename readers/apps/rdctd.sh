#!/bin/sh
#
# Sample the SBE-37SMP CTD and publish the data record to a Redis channel
#
: ${REDIS_HOST=localhost}
: ${REDIS_PORT=6379}
: ${CTD_INDEX=1}

[ -n "$CTD_HOST" ] || exit 0
[ -n "$CTD_PORT" ] || exit 0

[ -e /etc/cron-env ] && . /etc/cron-env

# Create Kermit script
kfile="/tmp/ksc.$$"
trap "rm -f $kfile;exit 0" 0 1 2 15
cat<<EOF > "$kfile"
set take echo off
set input echo off
set host $CTD_HOST $CTD_PORT /raw
output \13\13
input 2 S>
xif fail {
    output \13
    input 3 S>
    if fail exit 1 No response
}
clear
output ts\13
input 5 S>
if fail exit 1 No response
xecho \fstripx(\flop(\v(input), \10), \10)\10
output qs\13
exit 0
EOF

echo "Sampling CTD on ${CTD_HOST}:${CTD_PORT}"
# Take a sample, parse the data record, and write it to the database
t="$(date +%s)"
data="$(kermit $kfile)"
if [ $? = "0" ]; then
    redis-cli -h $REDIS_HOST -p $REDIS_PORT publish "rawdata.ctd.${CTD_INDEX}" "$t $data"
fi
