#!/bin/sh
#
# Sample the RD Workhorse ADCP and publish the data record to a Redis channel
#
: ${REDIS_HOST=localhost}
: ${REDIS_PORT=6379}

[ -n "$ADCP_HOST" ] || exit 0
[ -n "$ADCP_PORT" ] || exit 0

[ -e /etc/cron-env ] && . /etc/cron-env

# Create Kermit script
kfile="/tmp/ksc.$$"
trap "rm -f $kfile;exit 0" 0 1 2 15
cat<<EOF > "$kfile"
set take echo off
set input echo off
set host $ADCP_HOST $ADCP_PORT /raw
output +++
input 10 >
xif fail {
    output \B
    input 10 >
    if fail exit 1 No response
}
clear
output cs\13
input 60 >
if fail exit 1 No response
xecho \fstripx(\flop(\v(input), \10), >)\10
output cz\13
exit 0
EOF

echo "Sampling ADCP on ${ADCP_HOST}:${ADCP_PORT}"
# Take a sample, parse the data record, and write it to the database
data="$(kermit $kfile)"
if [ $? = "0" ]; then
    t="$(date +%s)"
    redis-cli -h $REDIS_HOST -p $REDIS_PORT publish "rawdata.adcp.1" "$t $data"
fi
