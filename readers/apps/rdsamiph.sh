#!/bin/sh
#
# Sample the SAMI-pH instrument
#
: ${REDIS_HOST=localhost}
: ${REDIS_PORT=6379}
: ${SAMIPH_BAUD=57600}
: ${SAMIPH_TSAMPLE=240}
: ${SAMIPH_INDEX=1}

[ -n "$SAMIPH_HOST" ] || exit 0
[ -n "$SAMIPH_PORT" ] || exit 0

[ -e /etc/cron-env ] && . /etc/cron-env

# Create Kermit script
kfile="/tmp/ksc.$$"
trap "rm -f $kfile;exit 0" 0 1 2 15
cat<<EOF > "$kfile"
set take echo off
set input echo off
set host $SAMIPH_HOST $SAMIPH_PORT /raw
output F5A\13
input 2 \13
if fail exit 1 No response
clear
output R\13
input 2 ^0A\13
if fail exit 1 Invalid response
clear input
input $SAMIPH_TSAMPLE \13
xecho \fstripx(\v(input), \13)\10
output F5\13
exit 0
EOF

echo "Sampling SAMI-pH on ${SAMIPH_HOST}:${SAMIPH_PORT}"
# Take a sample and (optionally) write it to the database
t="$(date +%s)"
data="$(kermit $kfile)"
if [ $? = "0" ]; then
    redis-cli -h $REDIS_HOST -p $REDIS_PORT publish "rawdata.samiph.${SAMIPH_INDEX}" "$t $data"
fi
