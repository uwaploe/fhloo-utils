#!/bin/sh
#
# Sample the SAMI-CO2 instrument
#
: ${REDIS_HOST=localhost}
: ${REDIS_PORT=6379}
: ${SAMICO2_INDEX=1}

[ -n "$SAMICO2_HOST" ] || exit 0
[ -n "$SAMICO2_PORT" ] || exit 0

[ -e /etc/cron-env ] && . /etc/cron-env

# Create Kermit script
kfile="/tmp/ksc.$$"
trap "rm -f $kfile;exit 0" 0 1 2 15
cat<<EOF > "$kfile"
set take echo off
set input echo off
set host $SAMICO2_HOST $SAMICO2_PORT /raw
output F5A\13
input 2 \13
if fail exit 1 No response
clear
output R\13
input 2 \13
if fail exit 1 Invalid response
clear input
input 120 \13
xecho \fstripx(\v(input), \13)\10
output F5\13
exit 0
EOF

echo "Sampling SAMI-CO2 on ${SAMICO2_HOST}:${SAMICO2_PORT}"
# Take a sample and (optionally) write it to the database
t="$(date +%s)"
data="$(kermit $kfile)"
if [ $? = "0" ]; then
    redis-cli -h $REDIS_HOST -p $REDIS_PORT publish "rawdata.samico2.${SAMICO2_INDEX}" "$t $data"
fi
