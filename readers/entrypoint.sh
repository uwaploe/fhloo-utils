#!/bin/sh

# Dump the environment variables so they can be accessed
# by cron jobs.
/usr/bin/env > /etc/cron-env

# Optionally override the default cron jobs
[[ -n $CRONJOBS ]] && [[ -e $CRONJOBS ]] && cp "$CRONJOBS" /etc/crontabs/root

exec /sbin/tini -- "$@"
